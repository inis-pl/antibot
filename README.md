# antibot

Install php5:

```
composer require inis/antibot:dev-release/php5
```

Install php7:

```
composer require inis/antibot:dev-release/php7
```

Check test/atibots_tests.php for some examples.

Configuration:

More than 5 calls within 20 seconds is a bot traffic
```
$CONF['ip_blocker']['ip_limit'] = 5;
$CONF['ip_blocker']['ip_ttl'] = 20;
```
More than 10 calls within 40 seconds from a subnet is a bot traffic
```
$CONF['ip_blocker']['subnet_limit'] = 10;
$CONF['ip_blocker']['subnet_ttl'] = 40;
$CONF['ip_blocker']['octets_per_subnet'] = 3;
```
Array of blacklisted IPs/subnets - always allow traffic
```
$CONF['ip_blocker']['whitelist'] = [];
```
Array of whitelisted IPs/subnets - always block traffic
```
$CONF['ip_blocker']['blacklist'] = [];
```
Some more criteria to pass traffic (language, country)
```
$CONF['ip_blocker']['accept_language_preg'] = '/pl/i';
$CONF['ip_blocker']['accept_country_preg'] = '/pl/i';
$CONF['ip_blocker']['reject_language_preg'] = '/en/i';
$CONF['ip_blocker']['reject_country_preg'] = '/us/i';
$CONF['ip_blocker']['reject_user_agent_preg'] = '/GoogleImageProxy/i';
```
Redis prefixes for your environment, keep it different
```
$CONF['ip_blocker']['check_redis_prefix'] = 'cl_';
$CONF['ip_blocker']['block_redis_prefix'] = '';
```

Run tests with:
```
./vendor/bin/phpunit --bootstrap vendor/autoload.php tests
```
Run code covarage (with xdebug installed) with:
```
./vendor/bin/phpunit --bootstrap vendor/autoload.php --coverage-html cc-report/ --whitelist src/ tests
```
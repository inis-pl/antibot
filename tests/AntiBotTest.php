<?php

use Inis\AntiBot\Map\InvalidityReasonMap;
use M6Web\Component\RedisMock\RedisMockFactory;
use PHPUnit\Framework\TestCase;
use Inis\AntiBot\AntiBot;
use Inis\AntiBot\Redis\PredisClientAdapter;
use Inis\AntiBot\Redis\RedisClientInterface;

final class AntiBotTest extends TestCase
{
    protected RedisClientInterface $redis;


    protected function setUp(): void
    {
        $factory = new RedisMockFactory();
        $myRedisMockClass = $factory->getAdapterClass(\Predis\Client::class);
        $this->redis = new PredisClientAdapter(new $myRedisMockClass());
    }

    protected function tearDown(): void
    {
        $this->redis->flushdb();
        unset($this->redis);
    }

    public function testShouldReturnIp()
    {
        $ip = "192.168.1.1";
        $CONF['ip_blocker'] = [];

        $_SERVER['REMOTE_ADDR'] = $ip;

        $antiBot = new AntiBot($this->redis, $CONF['ip_blocker']);
        $antiBot->handleRequest($_SERVER);

        $this->assertEquals($ip, $antiBot->getIp());
        unset($redisMock);
    }

    public function testShouldReturnIpFromProxy()
    {
        $ip = "127.0.0.1, 192.168.1.1";
        $CONF['ip_blocker'] = [];

        $_SERVER['REMOTE_ADDR'] = "127.0.0.1";
        $_SERVER['HTTP_X_FORWARDED_FOR'] = $ip;

        $antiBot = new AntiBot($this->redis, $CONF['ip_blocker']);
        $antiBot->handleRequest($_SERVER);

        $this->assertEquals("192.168.1.1", $antiBot->getIp());
    }

    public function testShouldBlockIpWhenLimitReached()
    {
        $ip = "192.168.1.1";
        $CONF['ip_blocker']['octets_per_subnet'] = 3;

        $CONF['ip_blocker']['ip_limit'] = 2;
        $CONF['ip_blocker']['ip_ttl'] = 2;
        $CONF['ip_blocker']['subnet_limit'] = 100;
        $CONF['ip_blocker']['subnet_ttl'] = 100;
        $i = 1;
        do {
            $_SERVER['REMOTE_ADDR'] = $ip;
            $antiBot = new AntiBot($this->redis, $CONF['ip_blocker']);
            $antiBot->handleRequest($_SERVER);
            $isBot = $antiBot->isBot();
            $ipInvalidityReason = $antiBot->getInvalidityReason();
            $i++;
            unset($antiBot);
        } while ($i <= $CONF['ip_blocker']['ip_limit'] + 1);

        $this->assertTrue($isBot);
        $this->assertEquals(InvalidityReasonMap::DYNAMIC_BLACK_LIST, $ipInvalidityReason);
    }

    public function testShouldNotBlockIpWhenLimitReachedWithFlagsDisabled()
    {
        $ip = "192.168.1.1";
        $CONF['ip_blocker']['octets_per_subnet'] = 3;

        $CONF['ip_blocker']['ip_limit'] = 2;
        $CONF['ip_blocker']['ip_ttl'] = 2;
        $CONF['ip_blocker']['subnet_limit'] = 100;
        $CONF['ip_blocker']['subnet_ttl'] = 100;
        $i = 1;
        do {
            $_SERVER['REMOTE_ADDR'] = $ip;
            $antiBot = new AntiBot($this->redis, $CONF['ip_blocker']);
            $antiBot->disableFlags()->handleRequest($_SERVER);
            $isBot = $antiBot->isBot();
            $ipInvalidityReason = $antiBot->getInvalidityReason();
            $i++;
            unset($antiBot);
        } while ($i <= $CONF['ip_blocker']['ip_limit'] + 1);

        $this->assertFalse($isBot);
    }

    public function testShouldBlockSubnetWhenLimitReached()
    {
        $subnet = "192.168.1";
        $CONF['ip_blocker']['octets_per_subnet'] = 3;

        $CONF['ip_blocker']['ip_limit'] = 100;
        $CONF['ip_blocker']['ip_ttl'] = 100;
        $CONF['ip_blocker']['subnet_limit'] = 3;
        $CONF['ip_blocker']['subnet_ttl'] = 5;

        $i = 1;
        do {
            $_SERVER['REMOTE_ADDR'] = $subnet . "." . $i;
            $antiBot = new AntiBot($this->redis, $CONF['ip_blocker']);
            $antiBot->handleRequest($_SERVER);
            $isBot = $antiBot->isBot();
            $ipInvalidityReason = $antiBot->getInvalidityReason();
            $i++;
            unset($antiBot);
        } while ($i <= $CONF['ip_blocker']['subnet_limit'] + 1);

        $this->assertTrue($isBot);
        $this->assertEquals(InvalidityReasonMap::DYNAMIC_BLACK_LIST, $ipInvalidityReason);
    }

    public function testShouldNotBlockIpWhenIpOnWhitelist()
    {
        $ip = "192.168.1.1";
        $CONF['ip_blocker']['octets_per_subnet'] = 3;

        $CONF['ip_blocker']['ip_limit'] = 2;
        $CONF['ip_blocker']['ip_ttl'] = 2;
        $CONF['ip_blocker']['subnet_limit'] = 100;
        $CONF['ip_blocker']['subnet_ttl'] = 100;
        $CONF['ip_blocker']['whitelist'] = [$ip];

        $i = 1;
        do {
            $_SERVER['REMOTE_ADDR'] = $ip;
            $antiBot = new AntiBot($this->redis, $CONF['ip_blocker']);
            $antiBot->handleRequest($_SERVER);
            $isBot = $antiBot->isBot();
            $ipInvalidityReason = $antiBot->getInvalidityReason();
            $i++;
            unset($antiBot);
        } while ($i <= $CONF['ip_blocker']['ip_limit'] + 1);

        $this->assertFalse($isBot);
        $this->assertNull($ipInvalidityReason);
    }

    public function testShouldNotBlockSubnetWhenSubnetOnWhitelist()
    {
        $subnet = "192.168.1";
        $CONF['ip_blocker']['octets_per_subnet'] = 3;

        $CONF['ip_blocker']['ip_limit'] = 100;
        $CONF['ip_blocker']['ip_ttl'] = 100;
        $CONF['ip_blocker']['subnet_limit'] = 3;
        $CONF['ip_blocker']['subnet_ttl'] = 5;
        $CONF['ip_blocker']['whitelist'] = [$subnet];

        $i = 1;
        do {
            $_SERVER['REMOTE_ADDR'] = $subnet . "." . $i;
            $antiBot = new AntiBot($this->redis, $CONF['ip_blocker']);
            $antiBot->handleRequest($_SERVER);
            $isBot = $antiBot->isBot();
            $ipInvalidityReason = $antiBot->getInvalidityReason();
            $i++;
            unset($antiBot);
        } while ($i <= $CONF['ip_blocker']['subnet_limit'] + 1);

        $this->assertFalse($isBot);
        $this->assertNull($ipInvalidityReason);
    }

    public function testShouldBlockIpWhenIpOnBlacklist()
    {
        $ip = "192.168.1.1";
        $CONF['ip_blocker']['octets_per_subnet'] = 3;

        $CONF['ip_blocker']['ip_limit'] = 2;
        $CONF['ip_blocker']['ip_ttl'] = 2;
        $CONF['ip_blocker']['subnet_limit'] = 100;
        $CONF['ip_blocker']['subnet_ttl'] = 100;
        $CONF['ip_blocker']['blacklist'] = [$ip];

        $_SERVER['REMOTE_ADDR'] = $ip;

        $antiBot = new AntiBot($this->redis, $CONF['ip_blocker']);
        $antiBot->handleRequest($_SERVER);
        $isBot = $antiBot->isBot();

        $this->assertTrue($isBot);
    }

    public function testShouldBlockSubnetWhenSubnetOnBlacklist()
    {
        $subnet = "192.168.1";
        $ip = "192.168.1.1";
        $CONF['ip_blocker']['octets_per_subnet'] = 3;

        $CONF['ip_blocker']['ip_limit'] = 100;
        $CONF['ip_blocker']['ip_ttl'] = 100;
        $CONF['ip_blocker']['subnet_limit'] = 3;
        $CONF['ip_blocker']['subnet_ttl'] = 5;
        $CONF['ip_blocker']['blacklist'] = [$subnet];

        $_SERVER['REMOTE_ADDR'] = $ip;

        $antiBot = new AntiBot($this->redis, $CONF['ip_blocker']);
        $antiBot->handleRequest($_SERVER);
        $isBot = $antiBot->isBot();

        $this->assertTrue($isBot);
        $this->assertEquals(InvalidityReasonMap::STATIC_BLACK_LIST, $antiBot->getInvalidityReason());
    }

    public function testShouldBlockIpWhenOnDynamicBlacklist(){
        $ip = "192.168.1.1";
        $CONF['ip_blocker']['octets_per_subnet'] = 3;

        $CONF['ip_blocker']['ip_limit'] = 2;
        $CONF['ip_blocker']['ip_ttl'] = 2;
        $CONF['ip_blocker']['subnet_limit'] = 100;
        $CONF['ip_blocker']['subnet_ttl'] = 100;
        $i = 1;
        do {
            $_SERVER['REMOTE_ADDR'] = $ip;
            $antiBot = new AntiBot($this->redis, $CONF['ip_blocker']);
            $antiBot->handleRequest($_SERVER);
            $isBot = $antiBot->isBot();
            $ipInvalidityReason = $antiBot->getInvalidityReason();
            $i++;
            unset($antiBot);
        } while ($i <= $CONF['ip_blocker']['ip_limit'] + 2);

        $this->assertTrue($isBot);
        $this->assertEquals(InvalidityReasonMap::DYNAMIC_BLACK_LIST, $ipInvalidityReason);
    }

    public function testShouldBlockIpWhenNotAcceptedLanguage(){
        $ip = "192.168.1.1";
        $CONF['ip_blocker']['reject_language_preg'] = '#test1#';

        $_SERVER['REMOTE_ADDR'] = $ip;
        $_SERVER['HTTP_ACCEPT_LANGUAGE'] = 'test2';

        $antiBot = new AntiBot($this->redis, $CONF['ip_blocker']);
        $antiBot->handleRequest($_SERVER);

        $this->assertTrue($antiBot->isBot());
        $this->assertEquals(InvalidityReasonMap::NOT_ACCEPTED_GEO, $antiBot->getInvalidityReason());
    }

    public function testShouldBlockIpWhenNotAcceptedCountry(){
        $ip = "192.168.1.1";
        $CONF['ip_blocker']['reject_country_preg'] = '#test1#';

        $_SERVER['REMOTE_ADDR'] = $ip;
        $_SERVER['GEOIP_COUNTRY_CODE'] = 'test2';

        $antiBot = new AntiBot($this->redis, $CONF['ip_blocker']);
        $antiBot->handleRequest($_SERVER);

        $this->assertTrue($antiBot->isBot());
        $this->assertEquals(InvalidityReasonMap::NOT_ACCEPTED_GEO, $antiBot->getInvalidityReason());
    }

    public function testShouldBlockIpWhenNotAcceptedUserAgentWithDisabledFlags(){
        $ip = "192.168.1.1";
        $CONF['ip_blocker']['reject_user_agent_preg'] = '#(WPImageProxy|YahooMailProxy|GoogleImageProxy)#i';

        $_SERVER['REMOTE_ADDR'] = $ip;
        $_SERVER['HTTP_USER_AGENT'] = 'IAmWPImageproxy';

        $antiBot = new AntiBot($this->redis, $CONF['ip_blocker']);
        $antiBot->disableFlags()->handleRequest($_SERVER);

        $this->assertTrue($antiBot->isBot());
        $this->assertEquals(InvalidityReasonMap::NOT_ACCEPTED_UA, $antiBot->getInvalidityReason());
    }

    public function testShouldNotBlockIpWhenLimitReachedButLanguageAccepted()
    {
        $ip = "192.168.1.1";
        $CONF['ip_blocker']['accept_language_preg'] = '#pl#i';
        $CONF['ip_blocker']['octets_per_subnet'] = 3;

        $CONF['ip_blocker']['ip_limit'] = 2;
        $CONF['ip_blocker']['ip_ttl'] = 2;
        $CONF['ip_blocker']['subnet_limit'] = 100;
        $CONF['ip_blocker']['subnet_ttl'] = 100;
        $i = 1;
        do {
            $_SERVER['REMOTE_ADDR'] = $ip;
            $_SERVER['HTTP_ACCEPT_LANGUAGE'] = 'pl_PL';
            $antiBot = new AntiBot($this->redis, $CONF['ip_blocker']);
            $antiBot->handleRequest($_SERVER);
            $isBot = $antiBot->isBot();
            $ipInvalidityReason = $antiBot->getInvalidityReason();
            $i++;
            unset($antiBot);
        } while ($i <= $CONF['ip_blocker']['ip_limit'] + 1);

        $this->assertFalse($isBot);
    }

    public function testShouldNotBlockIpWhenLimitReachedButCountryAccepted()
    {
        $ip = "192.168.1.1";
        $CONF['ip_blocker']['accept_country_preg'] = '#pl#i';
        $CONF['ip_blocker']['octets_per_subnet'] = 3;

        $CONF['ip_blocker']['ip_limit'] = 2;
        $CONF['ip_blocker']['ip_ttl'] = 2;
        $CONF['ip_blocker']['subnet_limit'] = 100;
        $CONF['ip_blocker']['subnet_ttl'] = 100;
        $i = 1;
        do {
            $_SERVER['REMOTE_ADDR'] = $ip;
            $_SERVER['GEOIP_COUNTRY_CODE'] = 'pl';
            $antiBot = new AntiBot($this->redis, $CONF['ip_blocker']);
            $antiBot->handleRequest($_SERVER);
            $isBot = $antiBot->isBot();
            $ipInvalidityReason = $antiBot->getInvalidityReason();
            $i++;
            unset($antiBot);
        } while ($i <= $CONF['ip_blocker']['ip_limit'] + 1);

        $this->assertFalse($isBot);
    }
}
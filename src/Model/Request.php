<?php
declare(strict_types=1);

namespace Inis\AntiBot\Model;

class Request
{
    private const IP_HEADERS = [
        'HTTP_FORWARDED',
        'HTTP_FORWARDED_FOR',
        'HTTP_X_FORWARDED',
        'HTTP_X_FORWARDED_FOR',
        'HTTP_CLIENT_IP'
    ];

    private string $ip;

    private string $subnet;

    private array $requestArray;


    private function __construct(string $ip, string $subnet, array $server)
    {
        $this->ip = $ip;
        $this->subnet = $subnet;
        $this->requestArray = $server;
    }

    public static function create(array $server, int $subnetOctets): self
    {
        $ip = static::findIp($server);
        $subnet = static::findSubnet($ip, $subnetOctets);

        return new static($ip, $subnet, $server);
    }

    private static function findIp(array $server): ?string
    {
        if (static::shouldSearchHeadersForIp($server)) {
            return static::getIpFromHeaders($server);
        } else {
            return $server['REMOTE_ADDR'];
        }
    }

    private static function findSubnet(string $ip, int $subnetOctets): string
    {
        return implode('.', array_slice(explode('.', $ip), 0, $subnetOctets));
    }

    private static function shouldSearchHeadersForIp(array $server): bool
    {
        return false === empty($server['HTTP_VIA']) ||
               false === empty($server['HTTP_X_FORWARDED_FOR']);
    }

    private static function getIpFromHeaders(array $server): ?string
    {
        $headersValues = [];
        foreach (static::IP_HEADERS as $header) {
            if (false === empty($server[$header])) {
                $headersValues[] = $server[$header];
            }
        }

        $ipFromHeader = array_shift($headersValues);
        $ipFromHeader = preg_replace("/for=/", "", $ipFromHeader);
        $ipArray = preg_split("/,\s*/", $ipFromHeader);
        $ipFromHeader = array_pop($ipArray);

        return $ipFromHeader;
    }

    public function getIp(): string
    {
        return $this->ip;
    }

    public function getSubnet(): string
    {
        return $this->subnet;
    }

    public function getRequestArray(): array
    {
        return $this->requestArray;
    }
}
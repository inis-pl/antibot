<?php
declare(strict_types=1);

namespace Inis\AntiBot\Redis;

class RedisClientAdapter implements RedisClientInterface
{
    private \Redis $client;


    public function __construct(\Redis $client)
    {
        $this->client = $client;
    }

    public function set(string $key, string $value)
    {
        return $this->client->set($key, $value);
    }

    public function get(string $key)
    {
        return $this->client->get($key);
    }

    public function exists(string $key): int
    {
        return $this->client->exists($key);
    }

    public function incr(string $key): int
    {
        return $this->client->incr($key);
    }

    public function expire(string $key, int $seconds): bool
    {
        return (bool) $this->client->expire($key, $seconds);
    }

    public function flushdb(): bool
    {
        return (bool) $this->client->flushDB();
    }
}
<?php
declare(strict_types=1);

namespace Inis\AntiBot\Redis;

interface RedisClientInterface
{
    public function set(string $key, string $value);
    public function get(string $key);
    public function exists(string $key): int;
    public function incr(string $key): int;
    public function expire(string $key, int $seconds): bool;
    public function flushdb(): bool;
}
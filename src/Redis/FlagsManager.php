<?php
declare(strict_types=1);

namespace Inis\AntiBot\Redis;

class FlagsManager
{
    private RedisClientInterface $storage;

    private string $checkPrefix;

    private string $blockPrefix;

    private int $ipTTL;

    private int $subnetTTL;

    private int $ipLimit;

    private int $subnetLimit;


    public function __construct(RedisClientInterface $storage, array $configuration)
    {
        $this->storage = $storage;
        $this->checkPrefix = $configuration['check_redis_prefix'] ?? '';
        $this->blockPrefix = $configuration['block_redis_prefix'] ?? '';
        $this->ipTTL = $configuration['ip_ttl'] ?? 0;
        $this->subnetTTL = $configuration['subnet_ttl'] ?? 0;
        $this->ipLimit = $configuration['ip_limit'] ?? 0;
        $this->subnetLimit = $configuration['subnet_limit'] ?? 0;
    }

    public function manageIpFlags(string $ip): void
    {
        $this->manageFlags($ip, $this->ipLimit, $this->ipTTL);
    }

    public function manageSubnetFlags(string $subnet): void
    {
        $this->manageFlags($subnet, $this->subnetLimit, $this->subnetTTL);
    }

    public function isIpDynamicallyBlacklisted(string $ip): bool
    {
        return (bool) $this->storage->exists($this->blockPrefix . $ip);
    }

    public function isSubnetDynamicallyBlacklisted(string $subnet): bool
    {
        return (bool) $this->storage->exists($this->blockPrefix . $subnet);
    }

    public function isClientBlockedByIpRequestLimit(string $ip): bool
    {
        return $this->isBlockedByRequestLimit($this->checkPrefix . $ip, $this->ipLimit);
    }

    public function isClientBlockedBySubnetRequestLimit(string $subnet): bool
    {
        return $this->isBlockedByRequestLimit($this->checkPrefix . $subnet, $this->subnetLimit);
    }

    private function isBlockedByRequestLimit(string $checkKey, int $limit): bool
    {
        return (int) $this->storage->get($checkKey) > $limit;
    }

    private function manageFlags(string $keySuffix, int $limit, int $ttl): void
    {
        $checkKey = $this->checkPrefix . $keySuffix;
        $keyCount = $this->storage->incr($checkKey);

        if (1 === $keyCount) {
            $this->storage->expire($checkKey, $ttl);
        } elseif ($limit === $keyCount) {
            $blockKey = $this->blockPrefix . $keySuffix;
            $this->storage->set($blockKey, date('Y-m-d H:i:s'));
        }
    }
}
<?php
declare(strict_types=1);

namespace Inis\AntiBot;

use Inis\AntiBot\Map\InvalidityReasonMap;
use Inis\AntiBot\Model\Request;
use Inis\AntiBot\Redis\FlagsManager;
use Inis\AntiBot\Redis\RedisClientInterface;

class AntiBot
{
    private const DEFAULT_CONFIGURATION = [
        'whitelist' => [],
        'blacklist' => [],
        'octets_per_subnet' => 2,
        'check_redis_prefix' => 'cl_',
        'block_redis_prefix' => 'clb_',
        'ip_ttl' => 1800,
        'subnet_ttl' => 1800,
        'ip_limit' => 10,
        'subnet_limit' => 10
    ];

    private array $configuration;

    private FlagsManager $flagsManager;

    private Request $request;

    private ?int $invalidityReason = null;

    private bool $manageFlags = true;


    public function __construct(RedisClientInterface $storage, array $configuration)
    {
        $this->configuration = array_merge(
            static::DEFAULT_CONFIGURATION, $configuration
        );
        $this->flagsManager = new FlagsManager($storage, $this->configuration);
    }

    public function handleRequest(array $server): void
    {
        $this->request = Request::create($server, $this->configuration['octets_per_subnet']);
        if($this->manageFlags){
            $this->flagsManager->manageIpFlags($this->getIp());
            $this->flagsManager->manageSubnetFlags($this->getSubnet());
        }
    }

    public function disableFlags(): self
    {
        $this->manageFlags = false;

        return $this;
    }


    public function enableFlags(): self
    {
        $this->manageFlags = true;

        return $this;
    }

    /**
     * @throws \LogicException
     */
    public function isBot(): bool
    {
        if (null === $this->request) {
            throw new \LogicException('You need to process request before checking if it is a fraud');
        }

        if (false === $this->isClientOnStaticWhitelist() && false === $this->isClientAcceptedByLanguageOrCountry()) {
            $this->invalidityReason = null;
            if ($this->isClientOnStaticBlacklist()) {
                $this->invalidityReason = InvalidityReasonMap::STATIC_BLACK_LIST;
            } else if (
                $this->isClientOnDynamicBlacklist() ||
                $this->isClientBlockedByIpRequestLimit() ||
                $this->isClientBlockedBySubnetRequestLimit()
            ) {
                $this->invalidityReason = InvalidityReasonMap::DYNAMIC_BLACK_LIST;
            } else if ($this->isClientBlockedByLanguageOrCountry()) {
                $this->invalidityReason = InvalidityReasonMap::NOT_ACCEPTED_GEO;
            } else if ($this->isClientBlockedByUserAgent()) {
                $this->invalidityReason = InvalidityReasonMap::NOT_ACCEPTED_UA;
            }

            return null !== $this->invalidityReason;
        }

        return false;
    }

    public function isClientOnStaticBlacklist(): bool
    {
        $blackList = $this->configuration['blacklist'];

        return in_array($this->getIp(), $blackList) ||
               in_array($this->getSubnet(), $blackList);
    }

    public function isClientOnStaticWhitelist(): bool
    {
        $whiteList = $this->configuration['whitelist'];

        return in_array($this->getIp(), $whiteList) ||
               in_array($this->getSubnet(), $whiteList);
    }

    public function isClientOnDynamicBlacklist(): bool
    {
        return $this->flagsManager->isIpDynamicallyBlacklisted($this->getIp()) ||
               $this->flagsManager->isSubnetDynamicallyBlacklisted($this->getSubnet());
    }

    public function isClientBlockedByLanguageOrCountry(): bool
    {
        $geoConstraints = [
            'HTTP_ACCEPT_LANGUAGE' => $this->configuration['reject_language_preg'] ?? null,
            'GEOIP_COUNTRY_CODE' => $this->configuration['reject_country_preg'] ?? null
        ];

        $request = $this->request->getRequestArray();
        foreach ($geoConstraints as $geoConstraintHeader => $geoConstraintRegex) {
            if (
                false === empty($geoConstraintRegex) &&
                false === empty($request[$geoConstraintHeader]) &&
                0 === preg_match($geoConstraintRegex, $request[$geoConstraintHeader])
            ) {
                return true;
            }
        }

        return false;
    }

    public function isClientAcceptedByLanguageOrCountry(): bool
    {
        $geoConstraints = [
            'HTTP_ACCEPT_LANGUAGE' => $this->configuration['accept_language_preg'] ?? null,
            'GEOIP_COUNTRY_CODE' => $this->configuration['accept_country_preg'] ?? null
        ];

        $request = $this->request->getRequestArray();
        foreach ($geoConstraints as $geoConstraintHeader => $geoConstraintRegex) {
            if (
                !empty($geoConstraintRegex) &&
                !empty($request[$geoConstraintHeader]) &&
                preg_match($geoConstraintRegex, $request[$geoConstraintHeader])
            ) {
                return true;
            }
        }

        return false;
    }

    public function isClientBlockedByUserAgent(): bool
    {
        $uaConstraints = [
            'HTTP_USER_AGENT' => $this->configuration['reject_user_agent_preg'] ?? null,
        ];

        $request = $this->request->getRequestArray();
        foreach ($uaConstraints as $uaConstraintHeader => $uaConstraintRegex) {
            if (
                false === empty($uaConstraintRegex) &&
                false === empty($request[$uaConstraintHeader]) &&
                preg_match($uaConstraintRegex, $request[$uaConstraintHeader])
            ) {
                return true;
            }
        }

        return false;
    }

    public function isClientBlockedByIpRequestLimit(): bool
    {
        return $this->flagsManager->isClientBlockedByIpRequestLimit($this->getIp());
    }

    public function isClientBlockedBySubnetRequestLimit(): bool
    {
        return $this->flagsManager->isClientBlockedBySubnetRequestLimit($this->getSubnet());
    }

    public function getIp(): string
    {
        return $this->request->getIp();
    }

    public function getSubnet(): string
    {
        return $this->request->getSubnet();
    }

    public function getInvalidityReason(): ?int
    {
        return $this->invalidityReason;
    }
}
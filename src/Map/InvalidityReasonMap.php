<?php
declare(strict_types=1);

namespace Inis\AntiBot\Map;

class InvalidityReasonMap
{
    const STATIC_BLACK_LIST = 0;
    const DYNAMIC_BLACK_LIST = 1;
    const NOT_ACCEPTED_GEO = 2;
    const NOT_ACCEPTED_UA = 3;
}